# Coding Katas

Ce dépôt est une collection de katas de code que j'apprécie. Certains ont été repris de sources existantes et traduits. D'autres ont été créés de toute pièce.

* [🐾 Animal show](katas/animal-show.md)
* [💎 Diamant](katas/diamond.md)
* [⏱️ Duration](katas/duration.md)
* [📐 Fibonacci](katas/fibonacci.md)
* [📢 Hello ](katas/hello.md)
* [🕹️ PacMan](katas/pacman.md)
* [🏭 String Processor](katas/string-processor.md)