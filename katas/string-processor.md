# 🏭 String Processor

## Énoncé

Le but est de créer un programme qui **transforme une chaîne de caractères**.

Lorsqu'on lui fournit une chaîne de caractères, ce programme applique dessus une série d'opérations, dans cet ordre :
* 1 étape de découpage
* 1 étape de transformation
* 1 étape de collage

### Découpage

On va découper la chaîne fournie en plusieurs blocs. Il existe 3 découpages possibles.

* **Séparateur** : on découpe selon le séparateur fourni. Par exemple :
	* Séparateur("-") : `abcde-fgh-ijkl` -> `[abcde,fgh,ijkl]`
	* Séparateur(""): `abcde` -> `[a,b,c,d,e]`

* **Par blocs de `N`** : on découpe par blocs de N caractères, en commençant par le début. Par exemple :
	* Bloc(5) : `abcdefghijklmnopqrstuvwxyz` -> `[abcde,fghij,klmno,pqrst,uvwxy,z]`
	* Bloc(1) : `abcde` -> `[a,b,c,d,e]`

* **Isolement des voyelles** : les voyelles sont isolées. Tous les autres caractères se trouvant entre les voyelles sont groupés.
	* `abcdE123iz48` -> `[a,bcd,E,123,i,z48]`

### Transformation

On va appliquer une transformation sur chaque bloc issu de l'étape de découpage. Il existe 3 transformations possibles.

* **Comptage** : on renvoie le nombre de caractères dans le bloc
	* `[abc,d]` -> `[3,1]`

* **Majuscules alternées** : chaque lettre dont l'index dans le bloc est impair est mise en majuscules. Les autres caractères sont inchangés.
	* `[abcd1,456ij,z]` -> `[aBcD1,456Ij,z]`

* **Alphabet** : chaque lettre est remplacée par sa position dans l'alphabet. Chaque nombre est remplacé par la lettre dont c'est la position dans l'alphabet. Les autres caractères sont inchangés.
	* `[a,bCD,y123-z]` -> `[1,234,25abc-26]`

### Collage

On va recoller les blocs suite à l'opération de transformation, pour avoir de nouveau une unique chaîne de caractères. Il existe 3 collages possibles.

* **Séparateur** : les blocs sont recollés avec le séparateur fourni pour recomposer une chaîne de caractères.
	* Séparateur("-") : `[abc,def,ghi]` -> `abc-def-ghi`
	* Séparateur("123"): `[abc,def,ghi]` -> `abc123def123ghi`
	
* **Inverse** : les blocs sont recollés dans l'ordre inverse, en partant de la fin :
	* `[abc,def,ghi]` -> `ghidefabc`
	
* **Liste numérotée** : les blocs sont préfixés par leur position dans la liste (commençant à 1) et séparés par un point.
	* `[abc,def,ghi]` -> `1abc.2def.3ghi`
	
Lorsqu'on veut **créer un transformateur**, on doit fournir la nature des 3 opérations.
On peut ensuite demander au transformateur de transformer autant de chaînes de caractères qu'on le souhaite.

### Exemples

#### Exemple 1
```
Transformateur {
	découpage: Bloc(3), 
	transformation: Comptage, 
	collage: Séparateur("_")
}
```

* je lui passe `abcdef`, il me renvoie `3_3`
* je lui passe `abcdefghij`, il me renvoie `3_3_3_1`

#### Exemple 2

```
Transformateur {
	découpage: Voyelles,
	transformation: Majuscules alternées,
	collage: Inverse
}
```

* je lui passe `abcdefghijklmnopqrstuvwxyz`, il me renvoie `zyvWxupQrStojKlMnifGhebCda`

#### Exemple 3

```
Transformateur {
	découpage: Séparateur("."),
	transformation: Alphabet,
	collage: Liste numérotée
}
```

* je lui passe `-a1b2.-c3d4.-e5f6`, il me renvoie `1-1a2b.2-3c4d.3-5e6f`

## Commentaire

* J'avais créé cet exercice comme une illustration pratique de l'utilité de la **composition** en programmation orientée objet.
* L'exercice se prêt bien à l'utilisation de la méthode Top-Down :
    * On crée un test pour un transformateur complet
    * On implémente ensuite les différentes étapes, indépendamment, avec la méthode TDD
    * Une fois les 3 premières opérations implémentées, on les assemble et on valide le premier test
    * On peut ensuite enchaîner soit avec un nouveau test de transformateur complet, soit directement avec l'implémentation en TDD des 6 opérations manquantes
* C'est un bon exercice pour s'entraîner à découper un problème complexe en plusieurs problèmes simples