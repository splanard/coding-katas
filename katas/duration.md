# Duration Format

Kata repris de [ Codewars](https://www.codewars.com/kata/52742f58faf5485cae000b9a).

## Énoncé

L'objectif est d'écrire un programme qui prend en entrée un **nombre de secondes** et l'affiche dans un **format compréhensible par un humain**.

La sortie du programme prend donc la forme d'une chaîne de caractères, en anglais, qui représente la durée fournie en faisant intervenir **différentes unités de temps**, de la seconde à l'année.

_Ex : s'il reçoit_ `228453`_, le programme doit renvoyer_ `2 days, 15 hours, 27 minutes and 33 seconds`.

Le nombre de secondes en entrée sera toujours un **entier positif**.

Les **unités de temps** à afficher sont : `year`, `day`, `hour`, `minute`, et `second`.

Une année contient 365 jours, un jour contient 24 heures, une heure contient 60 minutes et une minute contient 60 secondes.

Si le nombre de secondes reçu est 0, le programme affiche simplement `now`.

Les unités doivent respecter les règles de grammaire singulier/pluriel :

> 1 minute and 30 second ❌  
> 1 minute and 30 seconds ✔️  

La plus grande unité possible doit toujours être utilisée : 

> 61 seconds ❌  
> 1 minute and 1 second ✔️  

Chaque unité ne peut apparaître qu'une seule fois :

> 5 seconds and 1 second ❌  
> 6 seconds ✔️  

Les unités doivent être affichées par ordre décroissant :  

> 1 day and 2 years ❌  
> 2 years and 1 day ✔️  

L'affichage doit être formaté avec des virgules, et le mot `and` séparant les deux dernières valeurs : 

> 3 years 50 days 12 hours 4 minutes ❌  
> 3 years, 50 days, 12 hours, 4 minutes ❌  
> 3 years, 50 days, 12 hours and 4 minutes ✔️  
> 50 days and 4 minutes ✔️  
> 1 second  ✔️  

Une unité ne doit pas être affichée si sa quantité est zéro : 

> 1 minute and 0 seconds ❌  
> 1 minute ✔️  

## Commentaire

* Pas de grosse complexité algorithmique
* Un kata qui se prête très bien à la réalisation en TDD : des petits pas facile à identifier, des étapes de refacto presque systématiques.
* Pas évident d'avoir une solution élégante et facile à lire au premier coup d'oeil 😉