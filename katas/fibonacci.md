# 📐 Fibonacci

## Énoncé

En mathématiques, la suite de Fibonacci, très connue, est une suite de nombre entiers dans laquelle chaque terme est la somme des deux termes qui le précèdent.

L'objectif de ce rapide exercice est de développer un programme qui, en prenant en paramètre un nombre `n`, renvoie `F(n)` le terme d'indice `n` de la suite de Fibonacci.

De façon arbitraire, les deux premiers termes de la suite sont 0 et 1.

_À titre d'exemple, voici les premières valeurs de la suite de Fibonacci :_

| **n** | 0 | 1 | 2 | 3 | 4 | 5 | ... |
|---|---|---|---|---|---|---|---|
| **Fibonacci(n)** | 0 | 1 | 1 | 2 | 3 | 5 | ... |

## Commentaire

* Un classique, très rapide à implémenter
* Comme `F(n)` dépend de `F(n-1)` et `F(n-2)`, l'exercice peut être une introduction à la **récursivité**
* Mais le principal intérêt que je vois dans cet exercice est l'introduction à la **programmation dynamique**. Et la comparaison des performances entre un programme qui utilise la programmation dynamique et un qui utilise la récursivité, pour des termes d'indice élevé (pas trop élevé quand même si vous utilisez la récursivité : votre machine risquerait de souffrir 😅).