# 🐾 Animal show

## Énoncé

Modéliser/implémenter les notions suivantes en utilisant la programmation orientée objet :

- Un `Animal` possède un `nom` et une `couleur`
- Le `Chat`, la `Cigogne`, la `Grenouille` et le `Cameleon` sont des animaux.
- Une `Grenouille` est toujours verte, une `Cigogne` est toujours blanche.
- Un `Animal` peut se présenter. Il renvoie alors une chaîne de caractères de la forme suivante: `Je suis un/une <type d'animal> <couleur> et je m'appelle <nom>`. L'article et la couleur s'accordent (masculin/féminin). _Exemple: `Je suis une grenouille verte et je m'appelle Kermitt`._
- Lorsqu'on demande à un `Chat` de se présenter, il ne répond rien. Il ne va quand même pas faire quelque chose sur demande ! C'est un chat après tout...
- On peut jouer avec un `Chat` ou un `Cameleon`.
- Lorsqu'on joue avec un `Chat`, il est content et accepte de se présenter la prochaine fois qu'on le lui demande, mais **uniquement** la prochaine fois (il ne faut pas abuser quand même...).
- À chaque fois qu'on joue avec un `Cameleon`, il change de couleur. On ne peut pas lui dire quelle sera sa prochaine couleur, c'est lui qui décide. Mais, dans tous les cas, il ne reprend pas sa couleur actuelle.

### Quelques exemples

Quelques exemples de résultats attendus (en Java) :

```java
Grenouille kermitt = new Grenouille("Kermitt");
kermitt.presenteToi(); /* Doit renvoyer: "Je suis une grenouille verte et je m'appelle Kermitt" */
```

```java
Cigogne cigogne = new Cigogne("Longues Pattes");
cigogne.presenteToi(); /* Doit renvoyer: "Je suis une cigogne blanche et je m'appelle Longues Pattes" */
```

```java
Chat felix = new Chat("Félix", "noir");
felix.presenteToi(); /* Le chat ne répond pas... */
felix.jouer();
felix.presenteToi(); /* Doit renvoyer: "Je suis un chat noir et je m'appelle Felix" */
felix.presenteToi(); /* Le chat ne répond plus... */
```

```java
Cameleon cameleon = new Cameleon("Pascal", "vert");
cameleon.presenteToi(); /* Doit renvoyer: "Je suis un caméléon vert et je m'appelle Pascal" */
cameleon.jouer();
cameleon.presenteToi(); /* Pourrait renvoyer, par exemple: "Je suis un caméléon rouge et je m'appelle Pascal" */
```

### Extension d'énoncé

- `Animal` supplémentaire : l'`Elephant`.
- Un `Elephant` est toujours gris.
- Un `Elephant` est gros et bruyant. Lorsqu'il se présente, il le fait en majuscules !
- On peut jouer avec un `Elephant`, mais à ses risques et périls (il est gros et parfois maladroit) ! Lorsqu'on joue avec un `Elephant`, cela lève une exception dont la cause est `Vous venez de vous faire écraser par un éléphant`.
- Un `Elephant` adore lancer de la boue partout avec sa trompe. Il a donc une méthode spéciale qui prend en paramètre un autre `Animal`. Dans ce cas, la couleur de l'autre animal change pour `marron sale`, même si c'est une cigogne ou un éléphant, mais **PAS** si c'est une grenouille ! (la grenouille a le temps de plonger sous l'eau pour se protéger du jet de boue).
- Un `Cameleon` sale ne change plus **jamais** de couleur.

```java
Grenouille kermitt = new Grenouille("Kermitt");
kermitt.presenteToi(); /* Doit renvoyer: "Je suis une grenouille verte et je m'appelle Kermitt" */
Cameleon cameleon = new Cameleon("Bibi", "vert");
cameleon.presenteToi(); /* Doit renvoyer: "Je suis un caméléon vert et je m'appelle Bibi" */
Elephant babar = new Elephant("Babar");
babar.presenteToi(); /* Doit renvoyer: "JE SUIS UN ÉLÉPHANT GRIS ET JE M'APPELLE BABAR" */
babar.lancerBoue(cameleon);
cameleon.presenteToi(); /* Doit renvoyer: "Je suis un caméléon marron sale et je m'appelle Bibi" */
cameleon.jouer();
cameleon.presenteToi(); /* Doit renvoyer: "Je suis un caméléon marron sale et je m'appelle Bibi" */
babar.lancerBoue(kermitt);
kermitt.presenteToi(); /* Doit renvoyer: "Je suis une grenouille verte et je m'appelle Kermitt" */
```

## Commentaire

* Exercice permettant à des développeurs débutants de tester leur maîtrise des concepts de base de la programmation orientée objet.
* Il peut facilement être réalisé en TDD.
* Il peut être sujet à la (re)découverte d'un design pattern... (je vous laisse chercher lequel et pour quoi faire, si vous le souhaitez)