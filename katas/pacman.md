# 🕹️ PacMan

> Interaction utilisateur 🙋

> Long 🕑

## Énoncé

Étape par étape, vous devez recréer le jeu PacMan, dans une version simplifiée.

Pour ceux qui se sentent de démarrer avec ces seules informations, GO ! 🔥

Sinon, voici une proposition de découpage en étapes.

### Déplacement dans une grille

Faire déplacer un repère dans une grille 2D de dimensions données.

Dans l'exemple suivant, on peut déplacer l'étoile dans une grille 9x5 (actualiser l'affichage après chaque déplacement) :

```text
.........
.........
....*....
.........
.........
```

L'étoile ne doit pas pouvoir sortir de la zone.

### Récupération de pièces

La grille contient initialement des pièces. Le personnage (identifié par le repère) doit les récupérer. Les pièces disparaîssent lorsqu'elles ont été récupérées.

```text
oo*..oooo
oooo.oooo
oooo.oooo
ooooooooo
ooooooooo
```

La position initiale du repère ne contient pas de pièce.

### Compteur de pièces

On peut ensuite tenir les comptes du nombre de pièces collectées et l'afficher :

```text
coins: 4

oo*..oooo
oooo.oooo
oooo.oooo
ooooooooo
ooooooooo
```

### Fin de jeu

Le jeu se termine lorsque toutes les pièces ont été récupérées.

```text
coins: 45 > Well done!

.........
.........
.........
.........
........*
```

### Obstacles

Lors de l'initialisation de la grille, on peut ajouter des obstacles. Ces obstacles bloquent les déplacements du personnage et ne contiennent pas de pièce.

```text
coins: 4

oo*..Xooo
oooo.Xooo
oooo.Xooo
oXooooooo
oXXooooXX
```

Si des pièces sont inaccessibles (à cause des obstacles), la fin de jeu doit être déclenchée si toutes les pièces **accessibles** ont été récupérées.

```text
coins: 27 > Well done!

.....Xooo
.....Xooo
.....XXXX
.X......*
.XX....XX
```

Et c'est déjà pas mal !! 😅

Ceux qui sont motivés peuvent poursuivre et implémenter : 
- les **bords traversants** : le personnage sort d'un côté de la grille et réapparaît de l'autre
- les **fantômes** : des adversaires qui se déplacent eux aussi sur la grille pour essayer de rattraper le personnage (et là ça devient vraiment compliqué pour un kata...)

## Commentaire 

* En raison de l'interaction utilisateur requise (et donc la nécessité d'un affichage et d'entrées captées par le programme), l'exercice se prête mieux à un **projet front-end** (en HTML/JS natif, ou avec un framework front quelconque). Mais avec du Java Swing, on s'en sort quand même 😅.
* Exercice un peu long. Peut-être plus adapté à des **développeurs expérimentés**, à qui la complexité ne fait pas peur et qui développeront suffisamment vite pour que l'exercice tienne sur une journée max.
* Du fait de sa complexité, c'est une occasion de pratiquer la **séparation des responsabilités** dans le code : bien séparer dans le code la logique de jeu de l'affichage, par exemple.