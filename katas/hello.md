# 📢 Hello 

## Énoncé

Écrire un programme qui permet de saluer une personne, en prenant son prénom en paramètre. Par exemple, pour le paramètre `Hugo`, le programme doit donc renvoyer `Hello, Hugo!`.

Si le programme ne reçoit pas de prénom (une chaîne vide `""`), il doit renvoyer `Hello, stranger!`.

Si le programme reçoit un prénom en majuscules (`KATE` par exemple), le programme doit crier en retour : `HELLO, KATE!`.

Faire évoluer le programme pour pouvoir passer deux prénoms en paramètres. Dans ce cas, il doit saluer les deux personnes, tout en conservant les fonctionnalités existantes. Le cri est activé uniquement lorsque les **deux** prénoms sont en majuscules.

> `Calvin`, `Hobbes` -> `Hello, Calvin and Hobbes!`  
> `Calvin`, `HOBBES` -> `Hello, Calvin and HOBBES!`  
> `CALVIN`, `HOBBES` -> `HELLO, CALVIN AND HOBBES!`  

Dorénavant, si seulement un des prénoms est en majuscules, le programme doit séparer les salutations, en commençant par la salutation normale, puis la salutation criée.

> `Oliv`, `TOM` -> `Hello, Oliv! HELLO, TOM!`  
> `TOM`, `Olivier` -> `Hello, Olivier! HELLO, TOM!`  

Faire évoluer le programme pour pouvoir passer un nombre indéfini de prénoms. Dans ce cas, le programme salue tout le monde, en séparant les prénoms par des virgules, et en mettant `and` pour le dernier. Tout en conservant les fonctionnalités existantes.

> `Riri`, `Fifi`, `Loulou`, `PICSOU` -> `Hello, Riri, Fifi and Loulou! HELLO, PICSOU!`

Généraliser la gestion des personnes inconnues (chaînes vides reçues) et permettre d'en saluer plusieurs.

> `"Riri"`, `"Fifi"`, `"Loulou"`, `""`, `"PICSOU"` -> `Hello, Riri, Fifi, Loulou and stranger! HELLO, PICSOU!`  
> `"Calvin"`, `""`, `""`, `"HOBBES"` -> `Hello, Calvin and strangers! HELLO, HOBBES!`

Enfin, faire évoluer le programme pour changer la façon de saluer les personnes. Au lieu de renvoyer `Hello, ...`, le programme doit maintenant renvoyer `Good morning, ...`.

> `Riri`, `Fifi`, `Loulou`, `PICSOU` => `Good morning, Riri, Fifi and Loulou! GOOD MORNING, PICSOU!`

## Commentaire

* Pas de complexité algorithmique particulière
* Se prête très bien à la réalisation en TDD. L'étape de refactoring, en particulier, peut être réalisées après chaque étape d'implémentation.
* Si le refactoring a bien été fait au fil de l'eau, la dernière étape ne doit nécessiter que très peu de modifications dans le code (hors tests).