# 💎 Diamant

Kata traduit de [codingdojo.org](https://codingdojo.org/kata/Diamond/).

## Énoncé

Étant donnée une lettre, afficher un diamant commençant par `A` et présentant la lettre fournie au niveau le plus large.

Par exemple, pour la lettre `C`, le programme doit afficher :

```text
  A
 B B
C   C
 B B
  A
```

## Commentaire

* La complexité réside principalement dans l'algorithmie
* Même si on peut évidemment appliquer le TDD partout, ce kata n'est pas idéal pour s'y entraîner (pas beaucoup d'étapes intermédiaires, et les marches sont hautes)